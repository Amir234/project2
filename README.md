# Blog post project

Blog post application project.

# Installation

- Go the project folder: cd module4project/Module4project
- Install the Composer dependecies: composer install

- Config the environment: cp .env.example .env

Add your credentials to the .env file
Set up the DB: php cli/create-db.php

Run the application (dev mode): php -S localhost:8889 -t public

## Routes

-[POST]/v1/blog/posts
-[POST]/v2/blog/category
-[POST]/v3/blog/postsCategories
-[DELETE]/shop/cart/add/{remove}/{product-id}
- [GET]/shop/cart